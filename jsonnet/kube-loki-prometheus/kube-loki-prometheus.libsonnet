local loki = import 'kube-loki/kube-loki.libsonnet';

{
  manifests(promNamespace='monitoring', lokiNamespace='logging', promtailImageTag='latest', 
    promtailImagePullPolicy='Always', lokiImageTag='latest', lokiImagePullPolicy='Always',
    lokiStorageSize='', extraConfig={},)::
    local kpImp = (import 'kube-prometheus/kube-prometheus.libsonnet') + {
      _config+:: {
        namespace: promNamespace,
      },
    };
    
    local kp = kpImp + {
      _config+:: {
        grafana+:: {
          datasources: kpImp._config.grafana.datasources + [
            {
              name: 'loki',
              type: 'loki',
              access: 'proxy',
              org_id: 1,
              url: 'http://loki.' + lokiNamespace + '.svc:3100',
              version: 1,
              editable: false,
            },
          ],
        },
      } + extraConfig,
    };

    loki.manifests(namespace=lokiNamespace, lokiStorageSize=lokiStorageSize,
      lokiImageTag=lokiImageTag, lokiImagePullPolicy=lokiImagePullPolicy,
      promtailImageTag=promtailImageTag, promtailImagePullPolicy=promtailImagePullPolicy) +
    { ['00namespace-' + name]: kp.kubePrometheus[name] for name in std.objectFields(kp.kubePrometheus) } +
    { ['0prometheus-operator-' + name]: kp.prometheusOperator[name] for name in std.objectFields(kp.prometheusOperator) } +
    { ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
    { ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
    { ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
    { ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
    { ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
    { ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) }
  ,
}
