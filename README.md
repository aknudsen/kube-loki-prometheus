# Kube-Loki-Prometheus
A Jsonnet library for deploying Loki and Kube-Prometheus stack to Kubernetes.

```
./build.sh
kubectl apply -f manifests/
```
