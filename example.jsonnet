local lokiProm = import 'kube-loki-prometheus/kube-loki-prometheus.libsonnet';

function(lokiStorageSize='')
  lokiProm.manifests(lokiStorageSize=lokiStorageSize)
