#!/usr/bin/env bash
set -xeo pipefail

cd "${0%/*}"

rm -rf manifests
mkdir manifests

jsonnet --tla-str lokiStorageSize="$LOKI_STORAGE_SIZE" -J vendor -m manifests example.jsonnet | xargs -I{} sh -c 'cat {} | gojsontoyaml > {}.yaml; rm -f {}' -- {}
